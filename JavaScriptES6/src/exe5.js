const arr = [1, 2, 3, 4, 5, 6];

const [x,...y]  = arr;

console.log(x);
console.log(y);


function soma (...param){
    return param.reduce((ants,dps) => ants + dps);
}

console.log(soma(1,2));
console.log(soma(2,2,2,2));


const usuario = {
    nome: 'Diego',
    idade: 23,
    endereco: {
    cidade: 'Rio do Sul',
    uf: 'SC',
    pais: 'Brasil',
    }
   };

   
   const usuario1 = {...usuario,nome:'Gabriel'};
   const usuario2 = {...usuario,endereco:{...usuario.endereco,cidade:'Lontras'}};

   console.log(usuario);
   console.log(usuario1);
   console.log(usuario2);
