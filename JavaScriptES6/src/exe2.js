const usuarios = [
    { nome: 'Diego', idade: 23, empresa: 'Rocketseat' },
    { nome: 'Gabriel', idade: 15, empresa: 'Rocketseat' },
    { nome: 'Lucas', idade: 30, empresa: 'Facebook' },
   ];

   const idades = usuarios.map(usuario => usuario.idade);
   console.log(idades);

   const rocketseat = usuarios.filter(item =>{

    return item.idade >= 18 && item.empresa === 'Rocketseat';

   });

   console.log(rocketseat);

   const finder = usuarios.find(item =>{
        return item.empresa === 'Google';
   });

   console.log(finder);

const idade50 = usuarios.map(usuario => ({...usuario,idade: usuario.idade*2})).filter(item => item.idade <= 50);
console.log(idade50);