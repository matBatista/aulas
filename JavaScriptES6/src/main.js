import api from './api';

class App {
    constructor(){
        this.repositories = [];
        
        this.formEl = document.getElementById('repo-form');
        this.inputEl = document.querySelector('input[name=repository]');
        this.listEl = document.getElementById('repo-list');

        this.registerHandlers();
    }

    registerHandlers(){

        this.formEl.onsubmit = event => this.addRepositorio(event);
    }

    setLoading(loading = true){

        if(loading === true){
            let load = document.createElement('li');
            load.appendChild(document.createTextNode('Carregando'));
            load.setAttribute('id','loading');

            this.listEl.appendChild(load);
        }else{
            documento.getElementById('loading').remove();
        }

    }
    async addRepositorio(event){
        event.preventDefault();

        const repInput = this.inputEl.value;

        if(repInput.length === 0)
            return;

            this.setLoading();
try{
        const response = await api.get(`/users/${repInput}`);

        const {name,description,html_url,avatar_url} = response.data;
       
        this.repositories.push({
            name,
            description,
            avatar_url,
            html_url,
        });

        this.inputEl.value='';
        this.render();
    }catch{
        alert('NaoEncontrado');
    }
        // this.setLoading(false);
    }

    render(){

        this.listEl.innerHTML = '';

        this.repositories.forEach(repo =>{
            let img = document.createElement('img');
            img.setAttribute('src',repo.avatar_url);

            let name = document.createElement("strong");
            name.appendChild(document.createTextNode(repo.name));

            let desc = document.createElement("p");
            desc.appendChild(document.createTextNode(repo.description));

            let link = document.createElement("a");
            link.setAttribute('target','_blank');
            link.setAttribute('href',repo.html_url);
            link.appendChild(document.createTextNode('Acessar'));

            let li = document.createElement('li');
            li.appendChild(img);
            li.appendChild(name);
            li.appendChild(desc);
            li.appendChild(link);

            this.listEl.appendChild(li);
        });
    }
}

new App();