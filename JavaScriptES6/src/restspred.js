//REST

const usuario = {
    nome: 'Matue',
    idade: 23,
    empresa: 'Sastec'
};

const {nome, ...rest} = usuario;

console.log(nome);
console.log(rest);


const arr = [1,2,3,4,5];

const [a,b,...c] = arr;

console.log(a);
console.log(b);
console.log(c);

function soma(a,b, ...params){
    
    return params;
    // return params.reduce((total,next) => total + nex);
}

console.log(soma(1,3,4,5,6));

// SPREAD;

const arr1 = [1,2,3,4];
const arr2 = [5,6,7,8];

const spd = [...arr1,...arr2];

console.log(spd);

const usuario2 = {
    nome: 'Empresa',
    idade: 23,
    empresa: 'Sastec'
};

const usuarioFinal = {...usuario2,nome: 'Matheus'};

console.log(usuarioFinal);
