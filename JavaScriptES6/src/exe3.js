// 3.1
const arr = [1, 2, 3, 4, 5];
arr.map(function(item) {
 return item + 10;
});

const item10 = arr.map(item => item + 10);

// 3.2
// Dica: Utilize uma constante pra function
const usuario = { nome: 'Diego', idade: 23 };
const mostraIdade = usuario => usuario.idade;

// 3.3
// Dica: Utilize uma constante pra function
const nome = "Diego";
const idade = 23;

const mostraUsuario = (nome = 'matheus', idade = 20) => ({nome,idade});

mostraUsuario(nome, idade);
mostraUsuario(nome);

// 3.4
const promise = function() {
 return new Promise(function(resolve, reject) {
 return resolve();
 })
}

const promise = () => new Promise((resolve,reject) => resolve);
