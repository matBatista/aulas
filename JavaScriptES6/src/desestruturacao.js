const usuario = {
    nome: 'Matheus',
    idade: 22,
    endereco: {
        cidade: 'Sao Paulo',
        estado: 'SC',
    },
};

const { nome, idade, endereco:{cidade}} = usuario;

function mostraNome({nome,endereco:{cidade}}){
    console.log(nome,cidade);    
}

mostraNome(usuario);
