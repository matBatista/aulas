const minhaPromise = () => new Promise((resolve,reject)=>{
    setTimeout(() => {
        resolve('RESOLVE')
    }, 2000);
 });
 
 // async function exePromise(){
 //     console.log(await minhaPromise());
 //     console.log(await minhaPromise());
 //     console.log(await minhaPromise());
 // }
 
 
 
 const execPromise = async()=> {
     console.log(await minhaPromise());
     console.log(await minhaPromise());
 }
 
 execPromise().then();
 