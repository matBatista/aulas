class list{
    constructor(){
        this.data = [];
    }

    add(data){
        this.data.push(data);
        console.log(this.data);
    }
}


class todoList extends list{
    constructor(){
        super();

        this.usuario = 'Matheus';
    }

    mostraUsuario(){
        console.log(this.usuario);
    }
}

const minhaLista = new todoList();

document.getElementById('btnAdicionar').onclick = function(){
    minhaLista.add('todos');
}


class matematica{
    static soma (a,b){

        return a + b;
    }
    
}

console.log(matematica.soma(1,5));
