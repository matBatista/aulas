const usuario = {
    nome: 'Matheus',
    idade: 22,
    endereco: {
        cidade: 'Sao Paulo',
        estado: 'SC',
    },
};

//simples
const { nome, idade, endereco: {cidade,estado}} = usuario;
console.log(nome);    
console.log(cidade);
console.log(estado);

//fuction
function mostraInfo({nome, idade}){
    console.log(`Meu nome é ${nome} e tenho: ${idade} anos.`);
}
mostraInfo({nome:'matheus',idade:20});


function mostraUsuario({nome}){
    console.log(nome);
}

mostraUsuario(usuario);
