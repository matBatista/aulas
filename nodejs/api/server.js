const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const requireDIR = require('require-dir');

//iniciando o app
const app = express();
app.use(express.json());
app.use(cors());

//iniando o DB
mongoose.connect(
    'mongodb://localhost:27017/nodeapi',
     {useNewUrlParser:true}
);

requireDIR('./src/models');

// const Product = mongoose.model('Product');

//ROTAS
app.use('/api', require('./src/routes'));

app.listen(3001);
