const express = require('express');
const routes = express.Router();

const prodController = require ('../controllers/ProductController');
routes.get('/products', prodController.index);
routes.get('/products/:id',prodController.show);
routes.post('/products', prodController.store);
routes.put('/products/:id',prodController.update);
routes.delete('/products/:id', prodController.destroy);

module.exports = routes;