const express = require('express');

const routes = express.Router();

const tweetController = require ('./controllers/TweetController');
const likeController = require ('./controllers/likeController');

routes.get('/tweets', tweetController.index);
routes.post('/tweets', tweetController.store);
routes.post('/likes/:id',likeController.store);
// routes.get('/',(req,res)=> {
//     return res.send('Hello World!');
// });

module.exports = routes;