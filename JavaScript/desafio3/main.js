
var input = document.querySelector('input');
var button = document.querySelector('#btnIdade');

function checaIdade(idade) {
    
    return new Promise(function(resolve,reject){

        setTimeout(() => {
            
           return idade >= 18 ? resolve('Maior que 18') : reject('Menor que 18');

        }, 2000);
        
    });

   }

   function validadeIdade(){
       var idade = input.value;

       checaIdade(idade).then(function(n){
           console.log(n);
       }).catch(function(n){
           console.log(n);
       })
   }
    
   button.onclick = validadeIdade;