var ulElement = document.querySelector('#app ul');
var inputElement = document.querySelector('#app input');
var buttonElement = document.querySelector('#app button');


function limpaLista()
{
    ulElement.innerHTML = '';

    var liElement = document.createElement('li');
    var liText = document.createTextNode('Carregando...');
    liElement.appendChild(liText);
    ulElement.appendChild(liElement);
}
function renderErro(){
    ulElement.innerHTML = '';

    liElement = document.createElement('li');
    liText = document.createTextNode('Error!');
    liElement.appendChild(liText);
    ulElement.appendChild(liElement);
}
function renderList(repositorio){
    
    ulElement.innerHTML = '';

    for(repo of repositorio)
    {
        liElement = document.createElement('li');
        liText = document.createTextNode(repo.name);
        liElement.appendChild(liText);
        ulElement.appendChild(liElement);
    }

}
function adicionarLista(user){

    axios.get('https://api.github.com/users/'+user+'/repos')
    .then(function(resolve){
        renderList(resolve.data);
    })
    .catch(function(error){
        console.log(error.response.status);
    });
}


function render(){

    limpaLista();

    var user = inputElement.value;

    if(!user){ 
        alert('Usuario Inexistente');
    } 
    else{
        adicionarLista(user);
    }
}

buttonElement.onclick = render;