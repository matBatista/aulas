var list = document.querySelector('#app ul');
var input = document.querySelector('#app input');
var button = document.querySelector('#app button');

var todos = JSON.parse(localStorage.getItem('list_todos')) || []; 

function renderTodos(){
    list.innerHTML = '';

    for(todo of todos){
        var liElement = document.createElement('li');
        var liText = document.createTextNode(todo);
        
        var linkElement = document.createElement('a');
        
        linkElement.setAttribute('href','#');

        var pos = todos.indexOf(todo);
        linkElement.setAttribute('onclick','deleteTodo('+pos+')');

        var linkText = document.createTextNode('Excluir');
        linkElement.appendChild(linkText);

        liElement.appendChild(liText);
        liElement.appendChild(linkElement);
        list.appendChild(liElement);
    }
}

renderTodos();

function addTodo(){
    var todoText = input.value;
    
    todos.push(todoText);
    input.value = '';

    renderTodos();
    saveTodos();
}

button.onclick = addTodo;

function deleteTodo(pos){
    todos.splice(pos, 1 );
    renderTodos();
    saveTodos();
}

function saveTodos(){

    localStorage.setItem('list_todos', JSON.stringify(todos));
}