
var minhaPromise = function(){

    return new Promise(function(resolve,reject){
        var xhr = new XMLHttpRequest();
        xhr.open('GET','https://api.githubss.com/users/matBatista');
        xhr.send(null);

        xhr.onreadystatechange = function()
        {
            if(xhr.readyState === 4){
                if(xhr.status === 200){
                    resolve(JSON.parse(xhr.responseText));
                }
                else{
                    reject('Error na Requisição');
                }
            }
        }
    });
}

minhaPromise()
.then(function(resolve){
    console.log(resolve);
})
.catch(function(error){
    console.warn(error);
});
